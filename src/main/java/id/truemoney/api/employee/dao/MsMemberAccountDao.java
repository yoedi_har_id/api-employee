package id.truemoney.api.employee.dao;

import java.sql.Connection;

import id.truemoney.api.employee.model.MsMemberAccountModel;

public interface MsMemberAccountDao {
	public MsMemberAccountModel inquiryMemberData (String idMember);
	public int updateMemberAccount(String idMember, String NomorKartu, Connection conn);
}
