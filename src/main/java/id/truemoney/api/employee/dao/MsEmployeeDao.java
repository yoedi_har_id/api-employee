package id.truemoney.api.employee.dao;

import java.sql.Connection;

import id.truemoney.api.employee.model.EmployeeModel;

public interface MsEmployeeDao {
	public int saveEmployee(EmployeeModel employeeModel, Connection conn);
	public boolean isExist(String employeeID, Connection conn);
}
