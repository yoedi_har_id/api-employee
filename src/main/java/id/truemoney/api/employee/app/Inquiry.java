package id.truemoney.api.employee.app;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.UriUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import id.truemoney.api.employee.dao.MsMemberAccountDao;
import id.truemoney.api.employee.dao.impl.MsMemberAccountDaoImpl;
import id.truemoney.api.employee.model.MsMemberAccountModel;
import id.truemoney.api.employee.model.RequestModel;

@RestController
public class Inquiry {
	Gson gson = new GsonBuilder().create();
	Logger log = Logger.getLogger(Inquiry.class);
	MsMemberAccountDao msMemberAccountDao = new MsMemberAccountDaoImpl();
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

	@RequestMapping(value = "/inquiry", method = RequestMethod.POST, consumes = MediaType.ALL_VALUE)
	public @ResponseBody String inquiry(@RequestBody String json, @RequestHeader HttpHeaders header)
			throws UnsupportedEncodingException {
		String ipadd = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest()
				.getRemoteAddr();
		MsMemberAccountModel memberAccountModel = new MsMemberAccountModel();
		try {

			json = UriUtils.decode(json, "UTF-8").replace("=", "");
			log.info("[" + format.format(new Date()) + "] - IPADD : " + ipadd + " --> REQ : " + json);
			RequestModel req = gson.fromJson(json, RequestModel.class);
			memberAccountModel = msMemberAccountDao.inquiryMemberData(req.getIdMember());
			if (memberAccountModel.getNama() == null) {
				memberAccountModel.setResponseCode("INQ-001");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error inquiry ", e);
		}
		log.info("[" + format.format(new Date()) + "] - IPADD : " + ipadd + " --> RES : "
				+ gson.toJson(memberAccountModel));
		return gson.toJson(memberAccountModel);

	}
}
