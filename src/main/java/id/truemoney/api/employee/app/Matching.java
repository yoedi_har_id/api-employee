package id.truemoney.api.employee.app;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.UriUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import id.truemoney.api.employee.dao.MsEmployeeDao;
import id.truemoney.api.employee.dao.MsMemberAccountDao;
import id.truemoney.api.employee.dao.impl.MsEmployeeDaoImpl;
import id.truemoney.api.employee.dao.impl.MsMemberAccountDaoImpl;
import id.truemoney.api.employee.datasource.DataSourceConnectionPostgres;
import id.truemoney.api.employee.model.EmployeeModel;
import id.truemoney.api.employee.model.MsMemberAccountModel;
import id.truemoney.api.employee.model.RequestModel;

@RestController
public class Matching {
	Gson gson = new GsonBuilder().create();
	Logger log = Logger.getLogger(Inquiry.class);
	MsMemberAccountDao msMemberAccountDao = new MsMemberAccountDaoImpl();
	MsEmployeeDao msEmployeeDao = new MsEmployeeDaoImpl();
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

	@RequestMapping(value = "/update", method = RequestMethod.POST, consumes = MediaType.ALL_VALUE)
	public @ResponseBody String update(@RequestBody String json, @RequestHeader HttpHeaders header)
			throws UnsupportedEncodingException {
		MsMemberAccountModel memberAccountModel = new MsMemberAccountModel();
		EmployeeModel employeeModel = new EmployeeModel();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
		String ipadd = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest()
				.getRemoteAddr();
		
		try {
			json = UriUtils.decode(json, "UTF-8").replace("=", "").replaceAll("\\+", "");
			log.info("[" + format.format(new Date()) + "] - IPADD : " + ipadd + " --> REQ : " + json);

			RequestModel req = gson.fromJson(json, RequestModel.class);

			memberAccountModel = msMemberAccountDao.inquiryMemberData(req.getIdMember());

			employeeModel.setIdEmployee(req.getNoKartu());
			employeeModel.setNama(memberAccountModel.getNama());

			Connection conn = DataSourceConnectionPostgres.getInstance().getDataSource().getConnection();
			
			conn.setAutoCommit(false);
			
			int hasil = msMemberAccountDao.updateMemberAccount(req.getIdMember(), req.getNoKartu(), conn);

			memberAccountModel = new MsMemberAccountModel();
			if (hasil > 0) {
				boolean isEmployee = msEmployeeDao.isExist(employeeModel.getIdEmployee(), conn);
				if (!isEmployee) {
					hasil = msEmployeeDao.saveEmployee(employeeModel, conn);
					if (hasil > 0) {
						memberAccountModel.setResponseCode("UPD-000");
					} else {
						conn.rollback();
						memberAccountModel.setResponseCode("UPD-001");
					}
				} else {
					log.error("Employee is exist, isEmployee : "+isEmployee);
					conn.rollback();
					memberAccountModel.setResponseCode("UPD-001");
				}

			} else {
				log.error("Failed to update member, hasil : "+hasil);
				conn.rollback();
				memberAccountModel.setResponseCode("UPD-001");
			}
			if (conn != null) {
				conn.commit();
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error update ", e);
		}
		log.info("[" + format.format(new Date()) + "] - IPADD : " + ipadd + " --> RES : "
				+ gson.toJson(memberAccountModel));

		return gson.toJson(memberAccountModel);
	}
}
